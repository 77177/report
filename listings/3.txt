*** This is NuSMV 2.6.0 (compiled on Wed Oct 14 15:36:56 2015)
*** Enabled addons are: compass
*** For more information on NuSMV see <http://nusmv.fbk.eu>
*** or email to <nusmv-users@list.fbk.eu>.
*** Please report bugs to <Please report bugs to <nusmv-users@fbk.eu>>

*** Copyright (c) 2010-2014, Fondazione Bruno Kessler

*** This version of NuSMV is linked to the CUDD library version 2.4.1
*** Copyright (c) 1995-2004, Regents of the University of Colorado

*** This version of NuSMV is linked to the MiniSat SAT solver. 
*** See http://minisat.se/MiniSat.html
*** Copyright (c) 2003-2006, Niklas Een, Niklas Sorensson
*** Copyright (c) 2007-2010, Niklas Sorensson

WARNING *** The model contains PROCESSes or ISAs. ***
WARNING *** The HRC hierarchy will not be usable. ***
-- specification (((( F proc0.state = critical2 &  F proc1.state = critical2) &  F proc2.state = critical2) &  F proc3.state = critical2) &  F proc4.state = critical2)  is true
-- specification !( F ((((proc0.state = critical2 & proc1.state = idle) & proc2.state = idle) & proc3.state = idle) & proc4.state = idle))  is false
-- specification !( F ((((proc0.state = critical2 & proc1.state = critical2) & proc2.state = idle) & proc3.state = idle) & proc4.state = idle))  is false
-- specification !( F ((((proc0.state = critical2 & proc1.state = critical2) & proc2.state = critical2) & proc3.state = idle) & proc4.state = idle))  is false
-- specification !( F ((((proc0.state = critical2 & proc1.state = critical2) & proc2.state = critical2) & proc3.state = critical2) & proc4.state = idle))  is true
-- specification !( F ((((proc0.state = critical2 & proc1.state = critical2) & proc2.state = critical2) & proc3.state = critical2) & proc4.state = critical2))  is true
